# ext-theme-anegan-default How to Use

Attention: This theme is intended to be deployed in EXTJS 4 APPLICATIOn ONLY! Further development for Extjs 5 will be announced soon.

To use this theme and deploy it in your Extjs 4 application project, follow this steps:

1. Suppose your project directory is located in D drive, then go to your extjs root application directory. For example: '/Projects/Localhost/my-project-directory'. If you use silex, then the directory target should be like this '/Projects/Localhost/my-project-directory/web'
2. Go to 'ext/packages/'. That's the directory wheren extjs theme package is deployed.
3. Make new folder named 'ext-theme-anegan-default'.
4. Copy all the source from this git to the newly-created directory.
5. Go back to your extjs root application directory.
6. Open 'Bootstrap.css', then change the syntax to be like this 

```
#!css

@import 'ext/packages/ext-theme-anegan-default/build/resources/ext-theme-anegan-default-all.css';
```

7. For application building purpose, edit one line from '/.sencha/app/sencha.cfg' from


```
#!cfg

app.theme=ext-theme-classic
```
to be like this:


```
#!cfg

app.theme=ext-theme-anegan-default
```

That'all fellas. Now refresh your application url in your browser and see what you've got :D

Screenshot from TeKaPe:

Ext-Theme-Anegan-Default (clear blue sky)
![anegan-default.PNG](https://bitbucket.org/repo/oAxAXE/images/2504848113-anegan-default.PNG)

Ext-Theme-Anegan-Green
![anegan-green.PNG](https://bitbucket.org/repo/oAxAXE/images/3994909291-anegan-green.PNG)

Ext-Theme-Anegan-Orange
![anegan-orange.PNG](https://bitbucket.org/repo/oAxAXE/images/36237584-anegan-orange.PNG)

Ext-Theme-Anegan-Black
![anegan-black.PNG](https://bitbucket.org/repo/oAxAXE/images/887098100-anegan-black.PNG)

Ext-Theme-Anegan-Grey
![anegan-grey.PNG](https://bitbucket.org/repo/oAxAXE/images/3493416423-anegan-grey.PNG)

Ext-Theme-Anegan-Custom
![anegan-custom.PNG](https://bitbucket.org/repo/oAxAXE/images/1627458451-anegan-custom.PNG)